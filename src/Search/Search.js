import { useState } from 'react';
import './Search.css'
import axios from 'axios';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

function Search(props) {
    const [keyword, setKeyword] = useState("");
    const [language, setLanguage] = useState("");
    const [year, setYear] = useState(0);
    const [page, setPage] = useState(1);

    const [sort, setSort] = useState("");
    const [order, setOrder] = useState("ASC");
    const [total_pages, setTotalPages] = useState(0);
    const [jump_page, setJumpPage] = useState();

    let tokenStr = 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzNjg5NmYzNjViNmMyNmFjN2NiYTk2NmUzYzY4NGQwZSIsInN1YiI6IjY1MjhiNTc4NzAwYmY3MDEzYTVkYzRlYiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.3to0rjgFSG_kdpR12SvciYpHoO4m9LVERqGrGAHfxTg';

    function handleSearch() {
        const sendData = (mes)=>{
            props.parentCallback(mes);
        };

        let query = "https://api.themoviedb.org/3/search/movie?query=" + keyword;

        if (language !== "") {
            query += "&language=" + language;
        }

        if (year !== 0) {
            query += "&primary_release_year=" + year;
        }
        
        query += "&page="+page;

        let result = [];

        axios.get(query, { headers: {"Authorization" : `Bearer ${tokenStr}`} }).then((res)=>{            
            if (res.hasOwnProperty("data")) {
                result = res.data.results;

                if (sort === "alpha") {
                    if (order === "DESC") {
                        result.sort((a,b) => a.title.localeCompare(b.title));
                    } else {
                        result.sort((a,b) => b.title.localeCompare(a.title));
                    }
                } else if (sort === "date") {
                    if (order === "DESC") {
                        result.sort((a,b) => a.release_date.localeCompare(b.release_date));
                    } else {
                        result.sort((a,b) => b.release_date.localeCompare(a.release_date));
                    }
                } else if (sort === "rating") {
                    if (order === "DESC") {
                        result.sort((a,b) => a.vote_average - b.vote_average);
                    } else {
                        result.sort((a,b) => b.vote_average - a.vote_average);
                    }
                } else if (sort === "popularity") {
                    if (order === "DESC") {
                        result.sort((a,b) => a.popularity - b.popularity)
                    } else {
                        result.sort((a,b) => b.popularity - a.popularity)
                    }
                }

                setTotalPages(res.data.total_pages);
                sendData(result);
            }
            
        }).catch((error)=>{
            console.log(error);
        })
    }

    function handleFilterOpen() {
        const modal = document.getElementById("myModal");
        modal.style.display = "block";
    }

    function handleFilterSubmit() {
        const modal = document.getElementById("myModal");
        modal.style.display = "none";
        handleSearch();
    }

    function handleFilterClose() {
        setLanguage("");
        setYear(0);
        handleFilterSubmit();
    }

    function handleLanguageFilter() {
        const selectElement = document.getElementById("language_filter");
        setLanguage(selectElement.value);
    }

    function handleSort() {
        const selectElement = document.getElementById("sort_metric");
        setSort(selectElement.value);
        handleSearch();
    }

    function handleSortOrder() {
        const button = document.getElementById('order_button');
        if (button.textContent === "ASC") { 
            button.textContent = "DESC";
            setOrder("DESC");
        } else {
            button.textContent = "ASC";
            setOrder("ASC");
        }
        handleSearch();
    }

    function handlePrevPage() {
        setPage(page-1<1 ? 1 : page-1);
        handleSearch();
    }

    function handleNextPage() {
        setPage(page+1>total_pages ? total_pages : page+1);
        handleSearch();
    }

    function handlePageJump() {
        setPage(jump_page>total_pages ? total_pages : jump_page);
        handleSearch();
    }

    return (
        <div id='bar-container'>
            <div id='bar'>
                <div id='title'>Movie Archives</div>
                <div id='search-col'>
                    <div id='views'>
                        <Link to="/Gallery"><button>Gallery</button></Link>
                        <Link to="/"><button>List</button></Link>
                        <button id='filter' onClick={handleFilterOpen}>Filter</button>
                        <div id="myModal" className="modal">
                            <div className="modal-content">
                                <span className="close" id="closeModalBtn" onClick={handleFilterClose}>&times;</span>
                                <p className='filter_option'>Language: </p>
                                <select id='language_filter' onChange={handleLanguageFilter}>
                                    <option value=""></option>
                                    <option value="da">da</option>
                                    <option value="de">de</option>
                                    <option value="en">en</option>
                                    <option value="es">es</option>
                                    <option value="el">el</option>
                                    <option value="fr">fr</option>
                                    <option value="hi">hi</option>
                                    <option value="it">it</option>
                                    <option value="ja">ja</option>
                                    <option value="kn">kn</option>
                                    <option value="ko">ko</option>
                                    <option value="no">no</option>
                                    <option value="te">te</option>
                                    <option value="tl">tl</option>
                                </select>
                                <p className='filter_option'>Release year: </p>
                                <input type='text' value={year} onChange={e=>{e.persist(); setYear(e.target.value)}}/>
                                <button id='submit_filter' onClick={handleFilterSubmit}>Filter</button>
                            </div>
                        </div>
                    </div>
                    
                    <div id='search'>
                        <input type='text' placeholder='movie names to search' value={keyword} onChange={e=>{e.persist(); setKeyword(e.target.value)}}/>
                        <button onClick={handleSearch}>search</button>
                    </div>
                </div>
            </div>

            <div id='controller_container'>
                <div id='page_controller'>
                    <div></div>
                    <input id='page_input' type='number' placeholder='Jump to Page' value={jump_page} onChange={e=>{e.persist(); setJumpPage(e.target.value)}}/>
                    <button onClick={handlePageJump}>Go</button>
                    <div></div>
                    <button onClick={handlePrevPage} id='prev_page'>Prev Page</button>
                    <div id='page_num'>Page {page}/{total_pages}</div>
                    <button onClick={handleNextPage} id='next_page'>next Page</button>
                    <div></div>
                </div>

                <div id='sort_bar'>
                    <select defaultValue={"default"} id='sort_metric' onChange={handleSort}>
                        <option value="default" disabled>metrics to sort</option>
                        <option value="alpha">Alphabetic</option>
                        <option value="date">Date</option>
                        <option value="rating">Rating</option>
                        <option value="popularity">Popularity</option>
                    </select>
                    <button id='order_button' onClick={handleSortOrder}>ASC</button>
                </div>
            </div>
        </div>
    )
}

Search.protoTypes = {
    parentCallback: PropTypes.func.isRequired,
    keyword: PropTypes.string,
    language: PropTypes.string,
    page: PropTypes.number,
    sort: PropTypes.string,
    order: PropTypes.string,
    total_pages: PropTypes.number,
    jump_page: PropTypes.number,
    result: PropTypes.array
};


export default Search;
