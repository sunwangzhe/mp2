import './App.css'
import Search from "./Search/Search";
import List from './Display/List';
import Gallery from './Display/Gallery';
import Detail from './Detail/Detail';
import { useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  const [result, setResult] = useState({});
  let callbackFunction = (childData) => {
    setResult(childData);
  };


  return (
    <BrowserRouter basename={'/mp2'}>
      <Search parentCallback={callbackFunction}/>

      <Routes>
        <Route path='/' element={<List queryResult={result}/>} />
        <Route path='/Gallery' element={<Gallery queryResult={result}/>} />
        <Route path='/Detail' element={<Detail queryResult={result}/>} />
      </Routes>

    </BrowserRouter>
  );
}

export default App;
