import "./Detail.css"
import {Link, useSearchParams} from "react-router-dom";
import { useState, useEffect } from "react";
import axios from 'axios';
import PropTypes from 'prop-types';

function Detail(props) {
    let data = props.queryResult;
    const [searchParam] = useSearchParams();
    const [movieIndex, setMovieIndex] = useState(+searchParam.get("index"));

    const [movie, setMovie] = useState({});

    useEffect(()=>{
        const tokenStr = 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzNjg5NmYzNjViNmMyNmFjN2NiYTk2NmUzYzY4NGQwZSIsInN1YiI6IjY1MjhiNTc4NzAwYmY3MDEzYTVkYzRlYiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.3to0rjgFSG_kdpR12SvciYpHoO4m9LVERqGrGAHfxTg';
        const movie_id = data[movieIndex].id;
        axios.get(`https://api.themoviedb.org/3/movie/${movie_id}`, { headers: {"Authorization" : `Bearer ${tokenStr}`} }).then((res)=>{
            setMovie(res.data);
        }).catch((error)=>{
            console.log(error);
        })
    }, [movieIndex, data]);

    function handlePrev() {
        setMovieIndex(index => index-1<0 ? 0 : index-1);
    }

    function handleNext() {
        setMovieIndex(movieIndex+1>=data.length ? data.length-1 : movieIndex+1);
    }

    let genres = "";
    for (const key in movie.genres) {
        if (movie.genres.hasOwnProperty(key)) {
            if (genres === "") {
                genres = movie.genres[key].name;
            } else {
                genres += ", " + movie.genres[key].name;
            }
        }
    }

    let companies = "";
    for (const key in movie.production_companies) {
        if (movie.production_companies.hasOwnProperty(key)) {
            if (companies === "") {
                companies = movie.production_companies[key].name;
            } else {
                companies += ", " + movie.production_companies[key].name;
            }
        }
    }


    genres = genres.length===0 ? ["Unknown"] : genres;
    companies = companies.length===0 ? ["Unknown"] : companies;
    let adult = movie.adult===undefined ? "Unknown" : movie.adult.toString();


    return (
        <div>
            <div id="details">
                <div id="description">
                    <div id="poster">
                        <img id="poster" src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`} alt='poster'/>
                    </div>

                    <div id="title_attribute">
                        <div id="title">
                            <h1>{movie.original_title}</h1>
                        </div>
                        <div id="attribute">
                            <div>Language: {movie.original_language}</div>
                            <div>Release Date: {movie.release_date}</div>
                            <div>Adult: {adult}</div>
                            <div>Rating: {movie.vote_average}</div>
                            <div>Budget: {movie.budget}</div>
                            <div>Revenue: {movie.revenue}</div>
                            <div>Genres: {genres}</div>
                            <div>Production Companies: {companies}</div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="overview">
                <h2>Overview</h2>
                {movie.overview}
            </div>

            <button id="prev" onClick={handlePrev}>&lt;</button>
            <button id="next" onClick={handleNext}>&gt;</button>
            <Link id="back" to="/">Back</Link>
        </div>
    )
}

Detail.protoTypes = {
    queryResult: PropTypes.array.isRequired,
    movieIndex: PropTypes.number,
    movie: PropTypes.object
};

export default Detail
