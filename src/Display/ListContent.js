import { Link } from 'react-router-dom';
import './ListContent.css'
import PropTypes from 'prop-types';


function ListContent(props) {
    let data = props.queryResult;

    return (
        Object.keys(data).map(key => (
            <Link id='link' to={{pathname: `/Detail`, search: `index=${key}`}}>
                <div id='content'>
                    <div></div>
                    <div>{data[key].title}</div>
                    <div>{data[key].popularity.toFixed(1)}</div>
                    <div>{data[key].vote_average.toFixed(1)}</div>
                    <div>{data[key].release_date}</div>
                </div>
            </Link>
        ))
    )
}

ListContent.protoTypes = {
    queryResult: PropTypes.array.isRequired
};

export default ListContent;
