import './ListHeader.css'

function ListHeader() {
    return (
        <div>
            <div id='display_header'>
                <div></div>
                <div className='grid-item'>Title</div>
                <div className='grid-item'>Popularity</div>
                <div className='grid-item'>Rating</div>
                <div className='grid-item'>Release Date</div>
            </div>
        </div>
    )
}

export default ListHeader;
