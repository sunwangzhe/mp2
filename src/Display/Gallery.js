import { Link } from "react-router-dom";
import "./Gallery.css"
import PropTypes from 'prop-types';

function Gallery(props) {
    let data = props.queryResult;
    let imageGrid = [];

    for (const key in data) {
        if (data.hasOwnProperty(key)) {
            const res = data[key];
            if (res.poster_path != null) {

                const imageElement = (
                    <Link to={{pathname: `/Detail`, search: `index=${key}`}}>
                        <img className="gallery-item" src={`https://image.tmdb.org/t/p/w500/${res.poster_path}`} alt="Gallery-img"/>
                    </Link>
                )
        
                imageGrid.push(imageElement);
            }
        }
    }

    return (
        <div id="container">
            <div id="gallery-content">
                {imageGrid}
            </div>
        </div>
    );
}

Gallery.protoTypes = {
    queryResult: PropTypes.array.isRequired,
    data: PropTypes.array,
    imageGrid: PropTypes.array
};

export default Gallery;
