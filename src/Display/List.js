import ListContent from './ListContent';
import ListHeader from './ListHeader';

function List(props) {
    return (
        <div>
            <ListHeader />
            <ListContent queryResult={props.queryResult}/>
        </div>
    )
}

export default List;
